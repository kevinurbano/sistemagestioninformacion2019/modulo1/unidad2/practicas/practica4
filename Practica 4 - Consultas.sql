﻿USE practica4;

/* 1.- Averigua el DNI de todos los clientes. */
  SELECT 
    c.dni
    FROM 
      cliente c;

/* 2.- Consulta todos los datos de todos los programas. */
  SELECT 
    * 
    FROM 
      programa p;

/* 3.- Obtén un listado con los nombres de todos los programas. */
  SELECT 
    DISTINCT p.nombre 
    FROM 
      programa p;

/* 4.- Genera una lista con todos los comercios. */
  SELECT 
    * 
    FROM 
      comercio c;

/* 5.- Genera una lista de las ciudades con establecimientos donde se venden programas, 
       sin que aparezcan valores duplicados (utiliza DISTINCT). */
  SELECT 
    DISTINCT c.ciudad 
    FROM 
      comercio c;

/* 6.- Obtén una lista con los nombres de programas, sin que aparezcan valores duplicados (utiliza DISTINCT). */
  SELECT 
    DISTINCT p.nombre 
    FROM 
      programa p;

/* 7.- Obtén el DNI más 4 de todos los clientes. */
  SELECT 
    c.dni+4 
    FROM 
      cliente c;

/* 8.- Haz un listado con los códigos de los programas multiplicados por 7. */
  SELECT 
    p.codigo*7 
    FROM 
      programa p;

/* 9.- ¿Cuáles son los programas cuyo código es inferior o igual a 10? */
  SELECT 
    DISTINCT p.codigo,p.nombre,p.version
    FROM
      programa p 
    WHERE 
      p.codigo<=10;

/* 10.- ¿Cuál es el programa cuyo código es 11?  */
  SELECT 
    p.codigo,p.nombre 
    FROM 
      programa p 
    WHERE 
      p.codigo=11;

/* 11.- ¿Qué fabricantes son de Estados Unidos? */
  SELECT 
    f.id_fab,f.nombre
    FROM 
      fabricante f 
    WHERE 
      f.pais="Estados Unidos";

/* 12.- ¿Cuáles son los fabricantes no españoles? Utilizar el operador IN. */
  SELECT 
    f.id_fab,f.nombre 
    FROM 
      fabricante f 
    WHERE 
      f.pais NOT IN ("España"); 

/* 13.- Obtén un listado con los códigos de las distintas versiones de Windows. */
  SELECT 
    codigo
    FROM
      programa p
    WHERE
      p.nombre IN ("Windows"); 

/* 14.- ¿En qué ciudades comercializa programas El Corte Inglés? */
  SELECT 
    DISTINCT c.ciudad 
    FROM 
      comercio c 
    WHERE 
      c.nombre="El Corte Inglés";

/* 15.- ¿Qué otros comercios hay, además de El Corte Inglés? Utilizar el operador IN. */
  SELECT 
    c.cif,c.nombre 
    FROM 
      comercio c 
    WHERE 
      c.nombre NOT IN ("El Corte Inglés");

/* 16.- Genera una lista con los códigos de las distintas versiones de Windows y Access. Utilizar el operador IN. */
  SELECT 
    p.codigo
    FROM
      programa p
    WHERE
      p.nombre IN ("Windows","Access"); 

/* 17.- Obtén un listado que incluya los nombres de los clientes
        de edades comprendidas entre 10 y 25 y de los mayores de 50 años.
        Da una solución con BETWEEN y otra sin BETWEEN. */
  SELECT 
    DISTINCT nombre 
    FROM 
      cliente c 
    WHERE 
      c.edad BETWEEN 10 AND 25 
    OR 
      c.edad>50;

/* 18.- Saca un listado con los comercios de Sevilla y Madrid.
        No se admiten valores duplicados. */
  SELECT 
    c.cif, c.nombre 
    FROM
      comercio c 
    WHERE 
      c.ciudad IN ("Sevilla", "Madrid");

/* 19.- ¿Qué clientes terminan su nombre en la letra “o”? */
  SELECT 
    nombre 
    FROM 
      cliente c 
    WHERE 
      RIGHT(c.nombre,1)="o";

/* 20.- ¿Qué clientes terminan su nombre en la letra “o” y, además, son mayores de 30 años? */
  SELECT 
    nombre 
    FROM 
      cliente c 
    WHERE 
      RIGHT(c.nombre,1)="o"
    AND
      edad>30;

/* 21.- Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i,
        o cuyo nombre comience por una A o por una W. */
  SELECT 
    *
    FROM 
      programa p 
    WHERE 
      RIGHT(version,1)='i' 
    OR 
      LEFT(p.nombre,1) IN ('A','W');

/* 22.- Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i,
        o cuyo nombre comience por una A y termine por una S. */
SELECT 
  * 
  FROM 
    programa p
  WHERE
    RIGHT(p.version,1)='i'
  OR
    LEFT(p.nombre,1)='A' AND RIGHT(p.nombre,1)='S';

/* 23.-	Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i,
        y cuyo nombre no comience por una A. */
SELECT 
  p.codigo
  FROM 
    programa p
  WHERE 
    p.version LIKE '%i'
  AND 
    p.nombre NOT LIKE 'A%'; 

/* 24.- Obtén una lista de empresas por orden alfabético ascendente. */
SELECT 
  c.cif, c.nombre, c.ciudad
  FROM 
    comercio c 
  ORDER BY 
    c.nombre;

/* 25.- Genera un listado de empresas por orden alfabético descendente. */
SELECT 
  c.cif, c.nombre, c.ciudad
  FROM 
    comercio c 
  ORDER BY 
    c.nombre DESC;

/* 26.- Obtén un listado de programas por orden de versión. */
SELECT 
  p.codigo,p.nombre,p.version
  FROM 
    programa p 
  ORDER BY 
    p.version;

/* 27.- Genera un listado de los programas que desarrolla Oracle. */
SELECT 
  f.id_fab 
  FROM 
    fabricante f 
  WHERE 
    nombre='Oracle';

SELECT 
  d.codigo
  FROM 
    desarrolla d 
  JOIN 
    (
      SELECT f.id_fab FROM fabricante f WHERE nombre='Oracle'
    ) c1 ON d.id_fab=c1.id_fab;



/* 28.- ¿Qué comercios distribuyen Windows? */
SELECT 
  p.codigo 
  FROM 
    programa p 
  WHERE 
    p.nombre='windows';

SELECT 
  d.cif 
  FROM 
    distribuye d
  JOIN
    (SELECT p.codigo FROM programa p WHERE p.nombre='windows') c1
  ON d.codigo=c1.codigo;


/* 29.- Genera un listado de los programas y cantidades que se han distribuido a El Corte Inglés de Madrid. */
SELECT 
  c.cif 
  FROM 
    comercio c 
  WHERE 
    c.nombre='El Corte Inglés' AND c.ciudad='Madrid';

SELECT 
  d.codigo,d.cantidad
  FROM 
    distribuye d
  JOIN
    (
      SELECT 
        c.cif 
        FROM 
          comercio c 
        WHERE 
          c.nombre='El Corte Inglés' AND c.ciudad='Madrid'
    ) C1 ON d.cif=C1.cif;

/* 30.- ¿Qué fabricante ha desarrollado Freddy Hardest? */
SELECT 
  p.codigo 
  FROM 
    programa p 
  WHERE
    p.nombre='Freddy Hardest';

SELECT 
  d.id_fab 
  FROM 
    desarrolla d
  JOIN 
    (
      SELECT 
       p.codigo 
        FROM 
          programa p 
        WHERE
          p.nombre='Freddy Hardest'
    ) c1 ON d.codigo=c1.codigo;

/* 31.- Selecciona el nombre de los programas que se registran por Internet. */
SELECT 
  DISTINCT r.codigo 
  FROM 
    registra r 
  WHERE 
    r.medio='Internet';

SELECT 
  p.nombre
  FROM 
    programa p
  JOIN
    (
      SELECT 
        DISTINCT r.codigo 
        FROM 
          registra r 
        WHERE 
          r.medio='Internet'
    ) c1 ON p.codigo=c1.codigo;



/* 32.- Selecciona el nombre de las personas que se registran por Internet. */
SELECT 
  DISTINCT r.dni 
  FROM 
    registra r 
  WHERE 
    r.medio='Internet';

SELECT 
  c.nombre
  FROM 
    cliente c
  JOIN
    (
      SELECT 
        DISTINCT r.dni 
        FROM 
          registra r 
        WHERE 
          r.medio='Internet'
    ) c1 USING(dni);

/* 33.- ¿Qué medios ha utilizado para registrarse Pepe Pérez? */
SELECT 
  dni 
  FROM 
    cliente c 
  WHERE 
    c.nombre='Pepe Pérez';

SELECT 
  DISTINCT r.medio 
  FROM 
    registra r
  JOIN
    (
      SELECT 
        dni 
        FROM 
          cliente c 
        WHERE 
          c.nombre='Pepe Pérez'
    ) c1 USING(dni);

/* 34.- ¿Qué usuarios han optado por Internet como medio de registro? */
SELECT 
  r.dni 
  FROM 
    registra r 
  WHERE 
    r.medio='Internet';


/* 35.- ¿Qué programas han recibido registros por tarjeta postal? */
SELECT 
  r.codigo 
  FROM 
    registra r 
  WHERE 
    r.medio='Tarjeta postal';

SELECT 
  p.nombre,p.version 
  FROM
    programa p
  JOIN
    (
      SELECT 
      r.codigo 
      FROM 
        registra r 
      WHERE 
        r.medio='Tarjeta postal'
    ) c1 USING(codigo);

/* 36.- ¿En qué localidades se han vendido productos que se han registrado por Internet? */
SELECT 
  DISTINCT r.cif 
  FROM 
    registra r
  WHERE
    r.medio='Internet';

SELECT 
  DISTINCT c.ciudad
  FROM 
    comercio c
  JOIN
    (
      SELECT 
      DISTINCT r.cif 
      FROM 
        registra r
      WHERE
        r.medio='Internet'
    ) c1 USING(cif);

/* 37.- Obtén un listado de los nombres de las personas que se han registrado por Internet, 
        junto al nombre de los programas para los que ha efectuado el registro */
SELECT 
  dni,r.codigo 
  FROM 
    registra r 
  WHERE 
    r.medio='Internet';

SELECT 
  c.nombre,p.nombre 
  FROM 
    cliente c
  JOIN 
    (
      SELECT 
        dni,r.codigo 
        FROM 
          registra r
        WHERE 
          r.medio='Internet'
    ) c1 USING(dni)
  JOIN 
    programa p USING(codigo);

/* 38.- Genera un listado en el que aparezca cada cliente junto al programa que ha registrado,
        el medio con el que lo ha hecho y el comercio en el que lo ha adquirido. */
SELECT 
  c.nombre,p.codigo,r.medio,c1.cif 
  FROM 
    registra r
  JOIN
    cliente c USING(dni)
  JOIN
    comercio c1 USING(cif)
  JOIN
    programa p USING(codigo);

/* 39.- Genera un listado con las ciudades en las que se pueden obtener los productos de Oracle. */

-- C1
SELECT 
  f.id_fab 
  FROM 
    fabricante f 
  WHERE 
    f.nombre='Oracle';

-- C2
SELECT 
  DISTINCT d.codigo
  FROM 
    desarrolla d
  JOIN
    (
      SELECT 
      f.id_fab 
      FROM 
        fabricante f 
      WHERE 
      f.nombre='Oracle'
    ) c1 USING(id_fab);

-- C3
SELECT
  DISTINCT d.cif
  FROM 
    distribuye d  
  JOIN
    (
      SELECT 
        DISTINCT d.codigo
        FROM 
          desarrolla d
        JOIN
          (
            SELECT 
            f.id_fab 
            FROM 
              fabricante f 
            WHERE 
            f.nombre='Oracle'
          ) c1 USING(id_fab)
    ) c2 USING (codigo);

-- Final
SELECT 
  DISTINCT c.ciudad
  FROM 
    comercio c
  JOIN
    (
      SELECT
      DISTINCT d.cif
      FROM 
        distribuye d  
      JOIN
        (
          SELECT 
            DISTINCT d.codigo
            FROM 
              desarrolla d
            JOIN
              (
                SELECT 
                f.id_fab 
                FROM 
                  fabricante f 
                WHERE 
                f.nombre='Oracle'
              ) c1 USING(id_fab)
        ) c2 USING (codigo)
    ) c3 USING(cif);

/* 40.- Obtén el nombre de los usuarios que han registrado Access XP. */
-- C1
SELECT 
  p.codigo 
  FROM 
    programa p 
  WHERE 
    p.nombre='Access' 
  AND 
    LEFT(p.version,2)='XP';

-- C2
SELECT 
  r.dni 
  FROM 
    registra r
  JOIN
    (
      SELECT 
        p.codigo 
        FROM 
          programa p 
        WHERE 
          p.nombre='Access' 
        AND 
          LEFT(p.version,2)='XP'
    ) c1 USING(codigo);

-- Final
SELECT 
  c.nombre
  FROM 
    cliente c
  JOIN
    (
      SELECT 
        r.dni 
        FROM 
          registra r
        JOIN
          (
            SELECT 
              p.codigo 
              FROM 
                programa p 
              WHERE 
                p.nombre='Access' 
              AND 
                LEFT(p.version,2)='XP'
          ) c1 USING(codigo)
    ) c2 USING(dni);

/* 41.- Nombre de aquellos fabricantes cuyo país es el mismo que ʻOracleʼ. (Subconsulta). */
-- C1
SELECT 
  f.pais 
  FROM 
    fabricante f 
  WHERE 
    f.nombre='Oracle';

-- Final
SELECT 
  f.nombre
  FROM 
    fabricante f
  JOIN
    (
      SELECT 
      f.pais 
      FROM 
        fabricante f 
      WHERE 
        f.nombre='Oracle'
    ) c1 USING(pais)
  WHERE f.nombre<>'Oracle';

/* 42.- Nombre de aquellos clientes que tienen la misma edad que Pepe Pérez. (Subconsulta). */
-- C1
SELECT 
  c.edad 
  FROM 
    cliente c 
  WHERE 
    c.nombre='Pepe Peréz';

-- Final
SELECT 
  c.nombre
  FROM 
    cliente c
  JOIN
    (
      SELECT 
        c.edad 
        FROM 
          cliente c 
        WHERE 
          c.nombre='Pepe Peréz'      
    ) c1 USING(edad)
  WHERE c.nombre<>'Pepe Peréz';

/* 43.- Genera un listado con los comercios que tienen su sede en la misma ciudad
        que tiene el comercio ʻFNACʼ. (Subconsulta). */
-- C1
SELECT 
  c.ciudad 
  FROM 
    comercio c 
  WHERE 
    c.nombre='FNAC';

-- Final
SELECT 
  c.nombre 
  FROM 
    comercio c
  JOIN
    (
      SELECT 
      c.ciudad 
      FROM 
        comercio c 
      WHERE 
        c.nombre='FNAC'
    ) c1 USING(ciudad)
  WHERE
    c.nombre<>'FNAC'; 

/* 44.- Nombre de aquellos clientes que han registrado un producto de la misma forma que el cliente ʻPepe Pérezʼ. (Subconsulta). */
-- C1
SELECT 
  c.dni
  FROM 
    cliente c 
  WHERE 
    c.nombre='Pepe Pérez';

-- C2 
SELECT 
  r.medio 
  FROM 
    registra r 
  JOIN 
    (
      SELECT 
      c.dni
      FROM 
        cliente c 
      WHERE 
        c.nombre='Pepe Pérez'
    ) c1 USING(dni);

-- C3
SELECT 
  DISTINCT dni
  FROM 
    registra  
  WHERE 
    medio IN 
    (
      SELECT 
      r.medio 
      FROM 
        registra r 
      JOIN 
        (
          SELECT 
          c.dni
          FROM 
            cliente c 
          WHERE 
            c.nombre='Pepe Pérez'
        ) c1 USING(dni)
    )
  AND
    dni<>
    (
      SELECT 
      c.dni
      FROM 
        cliente c 
      WHERE 
        c.nombre='Pepe Pérez'
    );
    
-- Final
SELECT 
  DISTINCT c.nombre 
  FROM 
    cliente c
  JOIN
    (
      SELECT 
      dni
      FROM 
        registra  
      WHERE 
        medio IN 
        (
          SELECT 
          r.medio 
          FROM 
            registra r 
          JOIN 
            (
              SELECT 
              c.dni
              FROM 
                cliente c 
              WHERE 
                c.nombre='Pepe Pérez'
            ) c1 USING(dni)
        )
      AND
        dni<>
        (
          SELECT 
          c.dni
          FROM 
            cliente c 
          WHERE 
            c.nombre='Pepe Pérez'
        )
    ) c3 USING(dni);

/* 45.- Obtener el número de programas que hay en la tabla programas. */
SELECT 
  COUNT(*) nProgramas
  FROM 
    programa p;

/* 46.- Calcula el número de clientes cuya edad es mayor de 40 años. */
SELECT 
  COUNT(*) nClientes
  FROM 
    cliente 
  WHERE 
    edad>40;

/* 47.- Calcula el número de productos que ha vendido el establecimiento cuyo CIF es 1. */
SELECT 
  SUM(d.cantidad) nProductosVendidos 
  FROM 
    distribuye d 
  WHERE 
    d.cif='1';

/* 48.- Calcula la media de programas que se venden cuyo código es 7. */
SELECT 
  AVG(d.cantidad) mediaProgramas 
  FROM 
  distribuye d 
  WHERE 
    d.codigo='7';

/* 49.- Calcula la mínima cantidad de programas de código 7 que se ha vendido */
SELECT 
  MIN(d.cantidad) cantidadMinima
  FROM 
    distribuye d
  WHERE
    d.codigo='7';

/* 50.- Calcula la máxima cantidad de programas de código 7 que se ha vendido. */
SELECT 
  MAX(d.cantidad) cantidadMaxima
  FROM 
    distribuye d
  WHERE
    d.codigo='7';

/* 51.- ¿En cuántos establecimientos se vende el programa cuyo código es 7? */
SELECT 
  COUNT(DISTINCT cif) 
  FROM 
    distribuye d 
  WHERE 
    d.codigo='7';

/* 52.- Calcular el número de registros que se han realizado por Internet. */
SELECT 
  COUNT(*) nRegistros
  FROM 
    registra r 
  WHERE 
    r.medio='Internet';

/* 53.- Obtener el número total de programas que se han vendido en ʻSevillaʼ. */
-- C1
SELECT 
  cif 
  FROM 
    comercio c 
  WHERE 
    c.ciudad='Sevilla';

-- Final
SELECT 
  SUM(d.cantidad) nProgramas
  FROM 
    distribuye d
  JOIN
    (
      SELECT 
      cif 
      FROM 
        comercio c 
      WHERE 
        c.ciudad='Sevilla'
    ) c1 USING(cif);

/* 54.- Calcular el número total de programas que han desarrollado los fabricantes cuyo país es ʻEstados Unidosʼ. */
-- C1
SELECT 
  f.id_fab
  FROM 
    fabricante f 
  WHERE 
    f.pais='Estados Unidos';

-- Final
SELECT 
  COUNT(DISTINCT d.codigo)
  FROM 
    desarrolla d
  JOIN
    (
      SELECT 
      f.id_fab
      FROM 
        fabricante f 
      WHERE 
        f.pais='Estados Unidos'
    ) c1 USING(id_fab);

/* 55.- Visualiza el nombre de todos los clientes en mayúscula. 
        En el resultado de la consulta debe aparecer también la longitud de la cadena nombre. */
SELECT 
  UPPER(c.nombre) mayusNombre,
  CHAR_LENGTH(c.nombre) longitudNombre
  FROM 
    cliente c;

/* 56.- Con una consulta concatena los campos nombre y versión de la tabla PROGRAMA. */
SELECT 
  CONCAT(nombre,' ',version) nombre_version
  FROM 
    programa p;


